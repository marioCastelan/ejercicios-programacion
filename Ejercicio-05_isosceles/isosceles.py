# Autor: Castelan Hernandez Mario
# Fecha: 17 de Octubre de 2021
# Descripción: Programa que calcula el perímetro de un triángulo isósceles.


def calcula_perimetro(lado, base):
    return 2 * lado + base


def valida_numero(numero):
    # Revisa si es un entero
    if numero.isnumeric():
        return True
    else:
        # Revisa si es un numero real
        try:
            float(numero)
        except ValueError:
            print('No se ingreso un numero')
            return False
        return True


lado = input('Ingrese la longitud de un lado del triangulo isosceles: ')
base = input('Ingrese la longitud de la base del triangulo isosceles: ')
if valida_numero(lado) and valida_numero(base):
    print(f'{calcula_perimetro(float(lado), float(base))} Unidades')
