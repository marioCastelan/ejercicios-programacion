# Autor: Castelan Hernandez Mario
# Fecha: 15 de Octubre de 2021
# Descripción: Programa que recibe como entrada un monto en dólares (USD) y devuelve la cantidad equivalente en pesos mexicanos (MXN)

DOLAR_PESOS = 20.34


def dolares_a_pesos(dolares):
    return dolares * DOLAR_PESOS


def valida_numero(numero):
    # Revisa si es un entero
    if numero.isnumeric():
        return True
    else:
        # Revisa si es un numero real
        try:
            float(numero)
        except ValueError:
            print('No se ingreso un numero')
            return False
        return True


dolares = input('Monto en dólares (USD): ')
if valida_numero(dolares):
    print(f'{dolares_a_pesos(float(dolares))} MXN')
