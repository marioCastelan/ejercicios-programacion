# Autor: Castelan Hernandez Mario
# Fecha: 17 de Octubre de 2021
# Descripción: Programa que calcula el perímetro de un triángulo escaleno.


def calcula_perimetro(lado1, lado2, base):
    return lado1 + lado2 + base


def valida_numero(numero):
    # Revisa si es un entero
    if numero.isnumeric():
        return True
    else:
        # Revisa si es un numero real
        try:
            float(numero)
        except ValueError:
            print('No se ingreso un numero')
            return False
        return True


lado1 = input('Ingrese la longitud de un lado1 del triangulo escaleno: ')
lado2 = input('Ingrese la longitud de un lado2 del triangulo escaleno: ')
base = input('Ingrese la longitud de la base del triangulo escaleno: ')
if valida_numero(lado1) and valida_numero(lado2) and valida_numero(base):
    print(f'{calcula_perimetro(float(lado1), float(lado2), float(base))} Unidades')
