INICIO valida_numero
    ENTRADA numero
    
    SI numero ES entero:
        REGRESA TRUE
    SI numero ES flotante
        REGRESA TRUE
    SI NO:
        IMPRIME 'No se ingreso un numero'
        REGRESA FALSE
FIN valida_numero

INICIO calcula_perimetro
    ENTRADA longitud 
    REGRESA longitud * 3
FIN calcula_perimetro

INICIO
    ENTRADA longitud 
    SI valida_numero(longitud) = TRUE:
        IMPRIME calcula_perimetro(longitud) 'Unidades'
FIN