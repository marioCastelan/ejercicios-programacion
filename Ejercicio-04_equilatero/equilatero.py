# Autor: Castelan Hernandez Mario
# Fecha: 17 de Octubre de 2021
# Descripción: Programa que calcula el perímetro de un triángulo equilátero.


def calcula_perimetro(longitud):
    return longitud * 3


def valida_numero(numero):
    # Revisa si es un entero
    if numero.isnumeric():
        return True
    else:
        # Revisa si es un numero real
        try:
            float(numero)
        except ValueError:
            print('No se ingreso un numero')
            return False
        return True


longitud = input('Ingrese la longitud de un lado del triangulo equilatero: ')
if valida_numero(longitud):
    print(f'{calcula_perimetro(float(longitud))} Unidades')
