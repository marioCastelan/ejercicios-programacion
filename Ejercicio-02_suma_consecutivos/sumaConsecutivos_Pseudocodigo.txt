INICIO valida_numero
    ENTRADA numero
    
    SI numero  NO ES entero:
        IMPRIME 'No es un entero'
        REGRESA FALSE
    SI (numero < 1  OR numero > 50):
        IMPRIME 'Numero en rango no valido' 
        REGRESA FALSE
    REGRESA TRUE
FIN valida_numero


INICIO
    ENTRADA numero 
    VARIABLE suma ES numerico entero
    SI valida_numero(numero) = TRUE:
    PARA i = 1 HASTA i = 100:
        suma = suma + i 
    IMPRIME 'La suma es: ' suma 
FIN