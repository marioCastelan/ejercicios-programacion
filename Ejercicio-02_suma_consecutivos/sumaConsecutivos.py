# Autor: Castelan Hernandez Mario
# Fecha: 15 de Octubre de 2021
# Descripción: Programa que recibe un número entre 1 y 50 y devuelva la suma de los números consecutivos del 1 hasta ese número.

def validar_numero(numero):
    try:
        numero = int(numero)
    except(ValueError):
        print('No es un entero')
        return False
    if int(numero) < 1 or int(numero) > 50:
        print('Numero en rango no valido')
        return False
    return True


numero = input("Ingrese un número entero entre 1 y 50:\n")
suma = 0

if validar_numero(numero):
    for i in range(1, int(numero) + 1):
        suma += i
    print(f'La suma es: {suma}')
else:
    pass
