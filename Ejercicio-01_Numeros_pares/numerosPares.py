# Autor: Castelan Hernandez Mario
# Fecha: 15 de Octubre de 2021
# Descripción: Elabora un programa que imprima los números pares del 0 al 100.

_ = input()  # Espepra un enter para empezar a imprimir los pares del 0 al 100
numero_par = 0

while numero_par <= 100:
    print(numero_par)
    numero_par += 2
